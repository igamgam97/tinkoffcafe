import pandas


class LengthPredictor:

    def __init__(self):
        self.df = pandas.read_csv('pred_len.csv')

    def predict_for_user(self,user_id):
        return int(self.df.loc[user_id]['pred_len'])