import numpy as np
import pandas
from sklearn.feature_extraction.text import CountVectorizer


df = pandas.read_csv('train_with_menu - train_with_menu.csv')

count_vect = CountVectorizer(token_pattern=r'\d+')

df['len'] = df['good_id'].str.split().apply(len)


df2 = df.groupby('person_id')['len'].apply(list).astype(str)
some = count_vect.fit_transform(df2)
new_some = some / np.sum(some, axis=1)

new_df = pandas.DataFrame(data=new_some.A, index=pandas.unique(df['person_id']), columns=count_vect.get_feature_names())
new_df['pred_len'] = new_df.idxmax(axis=1)

new_df.to_csv('pred_len.csv')

