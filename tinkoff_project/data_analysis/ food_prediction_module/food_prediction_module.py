from predict_check_length import LengthPredictor
from work_with_data import MenuData
from surprise import dump
from surprise import SVD
import pandas as pd

class Predictor:
    def __init__(self, file_name='algo'):
        self.algo = self.get_algo()
        self.file_name = file_name


    def get_algo(self):
        _, loaded_algo = dump.load(self.file_name)
        return loaded_algo

    @staticmethod
    def predict(predictions, date):
        '''
            Надо сразу здесь генерировать датафрей, а то как аутисты, Кирилл
            сделай здесь сразу такой формат для предикта и норм
            '''

        top_n = pd.DataFrame()
        menu_data = MenuData()
        menu = menu_data.get_manu(date)
        for uid, iid, true_r, est, _ in predictions:
            if iid in menu:
                top_n[uid].append(iid)  # Здесь надо сделать что-то сделать как раз с добавлением элемента
                #   Работает это по идее так, добавляем все элементы итемов из меню
                #   Потом сортируем и берем топ н, подумай, Кирилл, че тут сделать можно
                #   Если ничего, оставим с твоим форматировщиком
        length_predictor = LengthPredictor()

        for uid, user_ratings in top_n.items():
            n = length_predictor.predict_for_user(uid)
            user_ratings.sort(key=lambda x: x[1], reverse=True)
            top_n[uid] = user_ratings[:n]

        return top_n
